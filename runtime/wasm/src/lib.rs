//! The Substrate node template runtime reexported for WebAssembly compile.

#![cfg_attr(not(feature = "std"), no_std)]

pub use local_template_node_runtime::*;
